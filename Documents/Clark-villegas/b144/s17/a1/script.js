

let student = [];
function addStudent(studentName) {
  console.log(`${studentName} was added to the student's list.`);
  return student.push(studentName);
}

function countStudents() {
  console.log(`There are a total of ${student.length} students enrolled`);
}


function printStudents(){
	student.sort();
	student.forEach(function(name){
		console.log(name)
	})
}

function findstudent(keyword){
	let match = student.filter(
		function(studentName){
			return studentName.toLowerCase().includes(keyword.toLowerCase())
		})

	if(match.length == 1){
		console.log(`${match} is an Enrollee`)
	}else if(match.length > 1){
		console.log(`${match} are Enrollees`)
	}else {
		console.log(`No student found with the name ${keyword}`)
	}

}
